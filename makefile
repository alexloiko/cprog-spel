FLAGS = -Wall -std=c++0x -ftrapv -Wconversion -pthread
DBG_FLAGS = -D_GLIBCXX_DEBUG -D_GLIBC_DEBUG -DDEBUG -DTHREADED_GAME  -g3 -ggdb 

default: test_main_env

%.o : %.cpp %.hpp
	g++ $(FLAGS) $(DBG_FLAGS) $< -c -o $@

test_main_env: Main.cpp Environment.o CharacterAction.o Game.o Character.o Item.o Player.o
	g++ $(FLAGS) $(DBG_FLAGS) $^ -o game