/* Föremålsklassen

   Ett föremål har antigen en position eller en ägare (hur garantera?). Den byter ägare när en character anropar drop eller take.
   Det är ägarens/rummets ansvar att kalla destruktorn (undvika minnesläcka).
   Items har egna attribut (inte bestämt vilka än, planen är olika slags bonusar, tex. +1 till attack. stats-systemet finns inte än)
   och en egen action() som körs varje tur och har tillgång till det publika i karaktären/rummet.

   Items ska kunna användas 

   Vissa items kan också öppna passager. (Fast det får nog passagerna avgöra)
   Vissa items kan bli uppätna av aktörer och då öka eller minska aktörens hitpoints.

   Items kan bli skadade av aktörer eller andra items endast om de finns med på dess lista över skadliga aktörer/items (gemensamma för klassen).
   Enklast är att alla skadar 1 hp; annars kan man istället för set ha en map med hitpoints också.
   Items har ingen action()-metod och kan därför bara agera när någon annan kallar deras metoder. 

   Dåligt: Det finns ingen begränsning på antal drag som i Character - man kan därför skapa ett svärd som istället för att såra draken en gång sårar den tio gånger i ett och samma drag. Kanske ska lägga till motsvarande begränsning? (Och motsvarande skydd i Environment)

 */

#ifndef ITEM_H
#define ITEM_H

// INCLUDES

#include <set>
#include <string>

// FORWARD DECLARATIONS
//class Passage;
class Environment;
class Character;
class Game;

class Item {

  friend class Game;
  int hitpoints;
  // vilka passager this kan öppna
  //std::set<Passage*> openable_passages;
  // vilka som kan skada this:
  //static std::set<Item*> dangerous_items;
  //static std::set<Character*> dangerous_characters;

  std::string i_description;

public:

  Item(std::string description);
  
  std::string description(); // typ "kniv" eller "äpple"

  // LOGIK

  //virtual void damage_by_item(Item & by_what)=0; // något skadar this - liv minskar
  //virtual void damage_by_character(Character & by_whom)=0; // någon skadar this - liv minskar

  /*
  virtual void open_passage(Passage & which)=0; // öppnar passager
  // flytta till Passage-klassen och låta den avgöra vilka items som öppnar dem??
  */

private:
  /*
    Returnerar sträng, typ "An apple a day keeps the doctor away!"
  */
  virtual std::string use_by(Character & whom); // ej const! Kan ändra whom

  
  /* Ha med dessa? Om riddaren sårar draken med ett svärd, behöver svärdet bli upplyst om det? Behöver svärdet godkänna handlingen som en korrekt?
     Dvs ska man ha med mellansteget här?
     knight.injure_with(dragon, sword) => sword.injure(dragon) => dragon.injure_by_item(sword)
  */
  /*
  virtual void cause_damage(Item & to_what )=0; // skada andra items
  virtual void injure(Character & whom)=0; // skada en aktör
  */
  
}; // class Item

#endif // ITEM_H
