#ifndef PASSAGE_H
#define PASSAGE_H

#include "Environment.hpp"

class Passage {
  Environment from;
  Environment to;
};

#endif // PASSAGE_H
