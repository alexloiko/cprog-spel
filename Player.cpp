#include "Player.hpp"


Player::Player(Character * chr, Game *the_game) : chr(chr), the_game(the_game) {}

void Player::request_action(CharacterAction * a) { the_game->request_action(a, chr); }

void Player::run() {
  while (chr != NULL) 
    request_action(CharacterAction::pass());
}
