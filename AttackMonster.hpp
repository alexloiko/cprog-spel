#include "Character.hpp"
#include <cassert>

class AttackMonster : public Character {

public:
  AttackMonster(std::set<Item*> initial_items,
                Environment * initial_location,
                std::string name) : Character(initial_items,
                                              initial_location, name, 4) {}

  std::string type() const {return "Brave Berserk Beast";}
  
  CharacterAction* action() {
    for (Character * chr : c_current_location->get_visible_chars(this))
      if (chr != this)
        return CharacterAction::attack(chr);
    return CharacterAction::pass();
  }
};
