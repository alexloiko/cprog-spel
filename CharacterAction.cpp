#include "CharacterAction.hpp"

//#define COMM(a) a
#define COMM(a, b) a, b

// GO
CharacterAction::CharacterAction(Environment* e) : 
  COMM(Action(CHARACTER),)
  act_type(actionType::GO), 
  env(e), chr(NULL), it(NULL), message("") {}
  

// ATTACK
CharacterAction::CharacterAction(Character* chr) :
  COMM(Action(CHARACTER),)
  act_type(actionType::ATTACK), 
  env(NULL), chr(chr), it(NULL), message("") {}

// TAKE, DROP, USE
CharacterAction::CharacterAction(actionType act_type, Item* it) :
  COMM(Action(CHARACTER),)
  act_type(act_type), 
  env(NULL), chr(NULL), it(it), message("") {}

// SAY
CharacterAction::CharacterAction(Character* chr, std::string message) :
  COMM(Action(CHARACTER),)
  act_type(actionType::SAY), 
  env(NULL), chr(chr), it(NULL), message(message) {}

// PASS
CharacterAction::CharacterAction() :
  COMM(Action(CHARACTER),)
  act_type(actionType::PASS), 
  env(NULL), chr(NULL), it(NULL), message("") {}
 
void CharacterAction::add_actor(Character* actor){
  character_subject = actor;
}

Character* CharacterAction::get_actor(){
  return character_subject;
}


// ACTIONS:

CharacterAction* CharacterAction::attack(Character * chr) {return new CharacterAction(chr);}


CharacterAction* CharacterAction::say(Character*chr, std::string message) {
  return new CharacterAction(chr, message);
}

CharacterAction* CharacterAction::take(Item* it) {
  return new CharacterAction(TAKE, it);
}

CharacterAction* CharacterAction::drop(Item* it) {
  return new CharacterAction(DROP, it);
}

CharacterAction* CharacterAction::use(Item* it) {
  return new CharacterAction(USE, it);
}

CharacterAction* CharacterAction::go(Environment* env)  {
  return new CharacterAction(env);
}

CharacterAction* CharacterAction::pass()  {
  return new CharacterAction();
}
