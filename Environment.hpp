#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include "EnvironmentAction.hpp"
#include "Character.hpp" // för att kunna deklarera Character::transfer_items() som friend

#include <set>

class Character;
class Item;

class Environment {


  // Game får ta bort och lägga till från in_current_location.
  friend class Game;


private:
  /*
  // used by Game class
  bool is_present_character(const Character&) const; // TODO
  bool is_present_item(const Item&) const; // TODO
  */

protected:
  std::set<Environment *> adjecent;
  std::set<Character *> in_current_location;
  std::set<Item*> items;

public:


  const std::set<Item*> get_items() const;
  
  /* returnerar in_current_location */
  virtual const std::set<Character *> get_visible_chars(Character*chr) const ;
  
  /* 
   Meningen är att klasser som ärver eventuellt skriver 
   över can_enter till t.ex.

   if (e == blue_door) {
     const std::set<Item*> c_items & = c->get_items();
     if (c_items.find(Blue_Key) == c_items.end())
       return false; // Not there!
     else 
       return true; // Hurray!
   }
   else { ...}

   Den enda som default-implementationen gör är att 
   titta efter om 'e' finns i adjecent

 */
  virtual bool can_enter(Character &c, Environment * e);

  virtual const std::set<Environment*> get_visible_adjecent(Character* ) const;

  // main needs access to adjecent to build environment
  friend int main();

private:
  /*
    Returns EnvironmentAction pointer
   */
  EnvironmentAction * action();

  // Default is do nothing
  virtual void act(void); 

  // kallas av Character-destruktorn som förflyttar alla sina items till rummet
  friend void Character::transfer_items();
};

#endif // ENVIRONMENT_H
