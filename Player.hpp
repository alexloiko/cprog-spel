/*
  Player class, 
*/
#ifndef PLAYER_H
#define PLAYER_H
#include "Character.hpp"
#include "Game.hpp"

//class Game;

class Player {

  friend class Game;
  friend int main() ;

protected:

  Character * chr;
  Game * the_game;

  Player(Character *, Game*);

  /*
    default is 'while (true) request_action();'
   */
  virtual void run();
  
  /*
    Anropar Game.request_action(någon action, chr)
  */
  void request_action(CharacterAction *);
};

#endif // PLAYER_H
