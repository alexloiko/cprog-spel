//#include "Game.hpp"
#include "Environment.hpp"
#include "TestMonster.hpp"
#include "AttackMonster.hpp"
#include "FailMonster.hpp"
#include "TestPlayer.hpp"
#include "TestItem.hpp"
//#include "Character.hpp"
//#include "Item.hpp"
//#include "Action.hpp"
#include "Game.hpp"
#include <set>

/* friend of Environment to build world */
int main() {
  
  /* Can't this be done in Player.cpp?*/
  TestPlayer::string2act_type["GO"]=GO;
  TestPlayer::string2act_type["ATTACK"]=ATTACK;
  TestPlayer::string2act_type["SAY"]=SAY;
  TestPlayer::string2act_type["USE"]=USE;
  TestPlayer::string2act_type["TAKE"]=TAKE;
  TestPlayer::string2act_type["DROP"]=DROP;
    
  Environment main_room, other_room;
  main_room.adjecent.insert(&other_room);
  other_room.adjecent.insert(&main_room);
  
  TestItem t1, t2,t3,t4;
  //std::set<Item*> = {&t1};

  TestMonster first_monster({&t1},&main_room, "Fluffy");
  AttackMonster second_monster({&t2},&main_room, "Plushy");
  FailMonster third_monster({&t3},&other_room, "Teddy");

  Character played_character({&t4}, &other_room, 
                             "Pico", 5, true);

  TestPlayer player(&played_character, NULL, std::cout, std::cin);
  main_room.in_current_location.insert(&first_monster);
  main_room.in_current_location.insert(&second_monster);
  other_room.in_current_location.insert(&third_monster);
  other_room.in_current_location.insert(&played_character);

  std::set<Item*> items;
  std::set<Environment*> envs;
  envs.insert(&main_room); envs.insert(&other_room);

  std::set<Character*> chars;
  chars.insert({&first_monster, &second_monster, &third_monster, &played_character});
  std::set<Player * > players;
  players.insert(&player);
  Game the_game(items, envs, chars, players);
  player.the_game = &the_game;
  while (true)
    the_game.play_threaded();
}
