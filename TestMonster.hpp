#include "Character.hpp"
#include <cassert>

//class Item;

class TestMonster : public Character {

public:
  TestMonster(std::set<Item*> initial_items,
              Environment * initial_location,
              std::string name) : 
    Character(initial_items,
              initial_location, name, 3) {}

  std::string type() const {return "Gargantuan Greeting oGre";}

  CharacterAction* action() {
    for (Character * chr : c_current_location->get_visible_chars(this))
      if (chr != this)
        return new CharacterAction(chr, "Hi!");
    assert(false);
    return new CharacterAction((Character*)NULL);
  }
};
