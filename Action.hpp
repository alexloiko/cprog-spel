/* class Action

   Används förslagsvis till:
   - i Player: tolkningen av kommandon - kommandon skickas till Character i form av en Action
   - i Character: 
   --- får ett action från Player ELLER skapar ett eget (om det inte är en spelare), 
   --- avgör om det är godkänt för instansen, 
   --- gör om det till funktionsanrop
   - i ??: sparas i eventlist

 */


#ifndef ACTION_H
#define ACTION_H

#include <cstddef> // NULL

class Character;
class Environment;
class Item;

enum action_kind{CHARACTER, ITEM, ENVIRONMENT};
class Action {

protected:
  // spara aktören:
  Character* character_subject;
  Environment* environment_subject;
  Item* item_subject;
  
public:
  action_kind kind_of_action;

  Action(action_kind kind_of_action) : character_subject(NULL), 
                                       environment_subject(NULL),
                                       item_subject(NULL),
                                       kind_of_action(kind_of_action) {}
  virtual ~Action() {};

  bool actor_is_dead() const{
    return character_subject == NULL && environment_subject == NULL && item_subject == NULL;
  }

  /*
  action_kind get_kind_of_action(){
    return kind_of_action;
    }*/

}; // class Action


#endif // ACTION_H
