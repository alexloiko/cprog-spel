#include "Character.hpp"
#include <cassert>

class FailMonster : public Character {

public:
  FailMonster(std::set<Item*> initial_items,
              Environment * initial_location,
              std::string name) : Character(initial_items,
                                            initial_location, name, 3) {}

  std::string type() const {return "Unspeakably Unsucessfull U???";}

  CharacterAction* action() { return CharacterAction::attack(this); }
};
