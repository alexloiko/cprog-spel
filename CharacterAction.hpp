/*
  CharacterAction, resultatet av Character::action().
  Har fältet act_type som kan vara 
  actionType::GO, TAKE, ATTACK, DROP, USE, SAY.
  --
  Används i Game, anropas från Game.
*/
#ifndef CHAR_ACTION_H
#define CHAR_ACTION_H

#include <string>
#include "Action.hpp"

/* Where should this be defined??? Inside the class?? As static? Namespace?
   use C++11-enum classes? (how do they work?)
*/

enum actionType {GO, TAKE, ATTACK, DROP, USE, SAY, PASS};
class Character;
class Environment;
class Item;
class Game;

class CharacterAction : public Action {
  friend class Game;
public:

  // GO
  CharacterAction(Environment*);

  // ATTACK
  CharacterAction(Character*);

  // TAKE, DROP, USE
  CharacterAction(actionType, Item*);

  // SAY
  CharacterAction(Character*, std::string);

  // PASS 
  CharacterAction();

  static CharacterAction* attack(Character*);
  static CharacterAction* say(Character*, std::string);
  static CharacterAction* take(Item*);
  static CharacterAction* drop(Item*);
  static CharacterAction* use(Item*);
  static CharacterAction* go(Environment*);
  static CharacterAction* pass();

  void add_actor(Character* );
  Character* get_actor();

  actionType act_type;
  Environment* env;
  Character* chr;
  Item* it;
  std::string message; // eftersom den skapas och inte redan finns.

};

#endif // CHAR_ACTION_H
