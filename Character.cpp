#include "Character.hpp"
#include "Game.hpp"
#include <cassert>

// RULE OF THREE:

Character::Character (std::set<Item*> initial_items, 
                      Environment * initial_location,
                      std::string c_name, int max_hitpoins,
                      bool is_player) :
  c_name(c_name), is_player(is_player), c_current_location(initial_location),
  c_hitpoints(max_hitpoins), c_max_hitpoints(max_hitpoins),
  c_items(initial_items) { }

Character::~Character(){
  // lämna items till current_location
  transfer_items();
  // TODO: meddela andra spelare om att dessa items har dykt upp i rummet?
}

void Character::transfer_items(){
  for (Item* item : c_items){
    this->c_items.erase(item);
    this->c_current_location->items.insert(item);
    }
}






/* 
   default action är pass, action() är inte const eftersom den 
   kan ändra state
*/
CharacterAction* Character::action() {return CharacterAction::pass();}

// SELECTORS:

const std::string & Character::name() const {return c_name;}
std::string Character::type() const {return "Monster";}

std::string Character::to_string() const {
  return name() + " the " + type();
}

const std::set<Item*> Character::items() const {return c_items;}


const Environment * Character::get_current_location() const { return c_current_location;}
