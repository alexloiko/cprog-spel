/* 
   Aktörsklassen, abstrakt basklass
 */
#ifndef CHARACTER_H
#define CHARACTER_H

// INCLUDES

#include <set>
#include <string>
#include <map>
#include "CharacterAction.hpp" // måste inkluderas eftersom vissa funktioner returnerar Action
#include "Item.hpp"


// FORWARD DECLARATIONS
class Passage;
class Player;
class Environment;

class Character {
  
  friend class Game;
  friend class Environment;
  friend class TestPlayer;
  //friend std::string Item::use_by(Character &);
  std::string c_name;  
  bool is_player;


protected:
  Environment * c_current_location;

public:
  const Environment * get_current_location() const;
  Character (std::set<Item*> initial_items, Environment * initial_location,
             std::string c_name, int max_hitpoins, bool is_player = false);

  virtual ~Character(); // lämnar items till current_location. Vem kör delete på spelarklassen?

  Character(const Character &) = delete; // förbjud kopieringskonstruktorn
  Character operator=(const Character &) = delete; // förbjud tilldelningsoperatorn tills vidare - kanske behövs senare?

private:

  //Player* player; // är null om ingen spelar denna character
  
  
  int c_hitpoints, c_max_hitpoints;
  
  std::set<Item*> c_items; // de items man har

protected:

  /* pga c++ medlemsaccess ser andra characters dina items*/
  const std::set<Item* > items() const ;
public:

  //bool is_player(); // player==NULL

  virtual std::string type() const;
  const std::string & name() const;
  
  std::string to_string() const ;

  /*
    default action är pass, note: ej const
  */
private:
  virtual CharacterAction* action();

  // kallas av destruktorn när man dör
  void transfer_items();


}; // class Character

#endif // CHARACTER_H
