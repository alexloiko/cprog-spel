class Environment {
    
    // Konstruktorer:
    Environment(map<Direction, Environment &> adjecent, 
    
private:
    set<Character &> currenly_present;
    
    set<Item &> items;
    
    /* Allt som händer sparas här och går att slå upp efter timeUnit.
      Varje händelse innehåller info över de relevanta aktörerna och en sträng för själva händelsen.
      Strunta i det här tillsvidare, fixar senare
      */
    map<timeUnit, pair<vector<Character &>, string > > eventList;
    
public:
    
    const set<Item &> & get_items() const { return items;}
    void remove_item(Item & what) { 
        if (!is_present(what)) {
            throw item_not_there_error;
            }
        items.erase(what);
    }
    
    
    /* Struntar i sånt som osynlighet och dyl. Alla kan se alla. */
    const set<Character & > & get_people_in_location() {
        return currently_present;
    }
    
    // ha med dessa? // sure, varför inte
    bool is_present(Character & char);
    bool is_present(Item & item);
    
        
    map<Direction, Environment  > adjecent; // behöver vara pekare, går inte att skapa något annars. Eller? börja med tomma kanske?
    
    void enter(Character & who, Direction fromWhere) {
        if (!is_present(who)) throw character_not_there_error;
        if fromWhere not in adjecent : throw something_strange_happened_error; // python!
        EventList.add({who}, who.to_string() + " entered the room from " + fromWhere.to_string());
     }
    
    void leave(Character & who, Direction where) {
        if (!is_present(who) throw character_not_there_error;
        currently_present.remove(who);
        
        if where not in adjecent : throw no_such_room_error;
        adjecent[where].enter(who, opposite(where));
        
        EventList.add({who}, who.to_string() + " left the room though " + where.to_string());
     }
    
    void attack(Character & who, Character & whom) {
        
        if (!is_present(who) || !is_present(whom)) throw character_not_there_error;
        EventList.add({who, whom}, who.to_string() + " attacked " + whom.to_string());
        whom.is_attacked_by(whom); // Hur spelas striderna ut?
    }
    
    
    };
class Character {
    Environment current_location;
    set<Item&> items; // najs, behövs i Environment med
    
protected:
    void take(Item & item){
        current_location.remove_item(item); // synlighet?? måste samtidigt garantera att man faktiskt tar det man tar från rummet, så att inte saker "försvinner" - minnesläcka // får bli metoder för det i Environment
        items.insert(item);
        };
    
    /* this säger till någon annan */
    void say_to(Character & char, std::string message){
        if (current_location.is_present(Char)){
            char.says(this, message); // tror det blir krångligt att hålla koll på. Tycker vi lägger den i EventList istället
        } else { // kasta upp nåt fel kanske?  // ok, kan lägga till det på alla andra ställen
        
        }
    }
    
    //private
   // bool done_action_this_turn
    
public:
    /* någon annan säger till this */
    /*
    void says(Character & char, std::string message){
        // skriv ut?
    }
    */
    
    // Klass lokal för Character och klasser som ärver
    class Action {
        
    }
    
    Action & perform_action(void) = 0; // implementeras i ärvda klasser, anropar någon av (ännu inte skrivna) funktioner attack/leave/talk
    
    };
