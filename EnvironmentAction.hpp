#ifndef ENV_ACTION_H
#define ENV_ACTION_H

#include "Action.hpp"

class Environment;

typedef void (*fpointer) (Environment*);
//typedef void (Environment::*fpointer) (void);

class EnvironmentAction : public Action {
public :
  fpointer act;
  EnvironmentAction(fpointer act, Environment* actor) : Action(ENVIRONMENT), act(act) 
  {
    environment_subject = actor;
  }

  Environment* get_actor(){
    return environment_subject;
  }
};

#endif
