/* 
   Huvudklassen
   
   Om man försöker döda draken med svärdet är det draken och inte svärdet som avgör om det är ett möjligt drag.
   Likaså om man försöker öppna en dörr med en nyckel avgör dörren om det är möjligt, inte nyckeln.
   Skador på aktörer och föremål mäts i hitpoints - enkla lösningen är att alla attacker (om de godkänds som möjliga) skadar lika mycket, annars kan man ha en map.

   En tanke var att vissa items, tex nycklar, kan öppna dörrar. Enklast är att då skapa en liten klass Passage som vet mellan vilka rum den går, om det är möjligt att passera och vad som annars kan öppna den. Rummen håller koll på passagerna ut från dem. Detta är inte implementerat, kan hoppa över om för komplicerat.

 */

#ifndef GAME_H
#define GAME_H
#include <set>
#include <string>
#include <mutex>
#include <queue>
#include <thread>

#include "Environment.hpp"
//#include "Item.hpp"
#include "Character.hpp"
//#include "Player.hpp"
#include "CharacterAction.hpp"
#include "Action.hpp"
class Main;
class Action;

class Game {
  // singleton

  std::mutex mtx;
  std::queue<Action*> q;
  friend class Main; // allt är privat, förutom requestAction
  
public:
  /* Main skapar alla miljöer och fyller dem med spelare/aktörer/föremål */
  Game(std::set<Item*>&, std::set<Environment*>&, std::set<Character*>&,
       std::set<Player*>&);

private:
  
  /* Testar om spelaren får utföra handlingen: att föremålet finns i rummet, att det 2:a rummet är intilliggande etc */
  //  bool checkAction(Character &, Action*); 
  bool checkAction(Action*);

  bool checkCharacterAction(CharacterAction* );

  int turn;
  
  /* Flyttar spelare mellan rum, tar upp objekt... */
  void performCharacterAction(Character &, CharacterAction) ;
  void performAction(Action*);
  
  
  /*
    varje Environment 
    vet vad som finns i dem och vilka andra locations de angränsar till
  */
  std::set<Environment*> locations; 
  
  /*
    Varje Character 
    vet vilken location de befinner sig i och vilka items de har
  */
  std::set<Character*> actors; 


  /*
    En player har en pekare till en Character som finns i actors
   */
  std::set<Player*> players;


  /* 
     Vill tillåta Environment att skapa/ta bort nya items - 
       * antingen göra items publik och inte ge Characters en Game-referens
       * eller ge Characters en Game-referens och lösa problemet med friend (hur?)
          och extra metoder add_item, remove_item.
     - Characters behöver ingen Game-referens!     
   */

  // PLAYING

  /* vektor-spel: gå igenom alla som har en action()-metod och köra den i turordning
   */

  std::set<Item*> items;


public:
  void play(); // går igenom alla rum och 1) kör action(), 2) kör play_room()

private:
  void play_room(Environment* ); // hjälpfunktion, kör action() på alla actors i rummet

public:

  // THREADED:
  /* trådat spel - automatic_run kallar action() på alla icke-spelare ex vart 3 sekund, och deras actions köas
     - spelare skickar förfrågningar till requestAction(Action*) som köar dem
     - main_run poppar ur kön och utför actions
     - trådskyddet ligger i request_action
 */
  // skapar två trådar för automatic_run resp. main_run
  void play_threaded();

private:

  void automatic_run(); // köar automatiska actions
  void automatic_run_room(Environment* ); // hjälpfunktion, går igenom alla i rummet
  void main_run(); // tömmer kön och utför actions

  void enqueue(Action* action); // intert, köar action som har ett subjekt

public:
  void request_action(CharacterAction* action, Character * who); // köar spelares actions

  
};

#endif // GAME_H



/* Kommandon 
go  rum-aktör
take/drop  aktör-item-rum
damage with aktör-item-item
injure with aktör-aktör-item
eat aktör-item
open item-rum
say aktör-aktör


 */
