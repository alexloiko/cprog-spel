#ifndef TEST_PLAYER_H
#define TEST_PLAYER_H

#include "Character.hpp"
#include "Player.hpp"
#include "CharacterAction.hpp"
#include <iostream>
#include <string>
#include <set>
#include <map>
#include <vector>

class TestPlayer : public Player {

  friend class Main;

  std::ostream &os;
  std::istream &is;
public:

  TestPlayer(Character * chr, Game * g, 
             std::ostream & os,  std::istream & is) :
             /*
               std::set<Item*> initial_items,
               Environment * initial_location, 
               std::ostream & os,
               std::istream & is,
               std::string c_name
             ) : Character(initial_items,
             initial_location, c_name, 5), os(os), is(is) {}*/
    Player(chr, g), os(os), is(is) {}

  //std::string type() const { return "Player Character";}
  
  static std::map<std::string, actionType> string2act_type;

  CharacterAction* action() {
    os << "You are " << chr->to_string() << " in " << chr->get_current_location() << std::endl
       << "Your items are:"  <<std::endl;
    std::vector<Item*> my_items, curr_loc_items;
    for (auto c: chr->items()) {
      os << my_items.size() << "\t" << c->description() << std::endl;
      my_items.push_back(c);
    }

    os << "The items in " << chr->get_current_location() << " are:" << std::endl;
    
    for (auto c: chr->c_current_location->get_items()) {
      os << curr_loc_items.size() << "\t" << c->description() << std::endl;
      curr_loc_items.push_back(c);
    }
            
    os << "List of players in current location:" << std::endl;
    std::vector<Character*> chars;
    for (auto c : chr->c_current_location->get_visible_chars(chr)) {
      os << chars.size() << "\t" << c->to_string() << std::endl;
      chars.push_back(c);
    }
    os << "---------" << std::endl << std::endl;
    
    os << "List of adjecent locations:" << std::endl;
    std::vector<Environment*> locs;
    for (auto e: chr->c_current_location->get_visible_adjecent(chr)) {
      os << locs.size() << "\t" <<  e << std::endl;
      locs.push_back(e);
    }
    
    os << "---------" << std::endl << std::endl;
    os << "Type the action you want to perform (eg. GO/SAY/ATTACK)\n> " << std::endl;
    std::string action;
    is >> action;
    actionType at = TestPlayer::string2act_type.at(action);
    os << "Action is " << at << std::endl;
    CharacterAction* ret = new CharacterAction();
    int pos;
    ret->act_type=at;
    switch (at) {
    case GO:
      os << "Type which location, 0 <= location < " << locs.size() << std::endl
         << "> ";
      is >> pos;
      ret->env=locs[pos];
      break;

    case TAKE:
      //assert(false);
      os << "Type which item, 0 <= item < " << curr_loc_items.size() 
         << "\n> ";
      is >> pos;
      ret->it = curr_loc_items[pos];
      break;
    
    case ATTACK:
      os << "Type which character, 0 <= character < " << chars.size() << std::endl
         << "> ";
      is >> pos;
      ret->chr=chars[pos];
      break;

    case DROP:
      //assert(false);
      os << "Type which item, 0 <= item < " << my_items.size() << std::endl
         << "> ";
      is >> pos;
      ret->it = my_items[pos];
      //is >> pos;
      //is >> ret.it;
      break;
      
    case USE:
      //assert(false);
      os << "Type which item, 0 <= item < " << my_items.size() << std::endl
         << "> ";
      is >> pos;
      ret->it = my_items[pos];
      //is >> ret.it;
      break;

    case SAY:
      os << "Type which character, 0 <= character < " << chars.size() << std::endl 
         << "> ";
      is >> pos;
      ret->chr=chars[pos];
      os << "Type your message to " << ret->chr << std::endl <<"> ";
      is >> ret->message;
      break;
      
    case PASS:
      break;
    }
    return ret;
  }

  void run() {
    while (chr!= NULL) 
      request_action(action());
  }
};

std::map<std::string, actionType> TestPlayer::string2act_type = 
          std::map<std::string, actionType>();

/*
TestPlayer::string2act_type["GO"]=GO;
TestPlayer::string2act_type["ATTACK"]=ATTACK;
*/

#endif // TEST_PLAYER_H
