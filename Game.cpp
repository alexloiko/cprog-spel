#include "Game.hpp"
#include <cassert>
#include <iostream>
#include <chrono> // sleep
#include "Player.hpp"

/*
  Alla miljöer skapas och fylls i Main.
 */
Game::Game(std::set<Item*> & i,
           std::set<Environment*> &e,
           std::set<Character*> &c,
           std::set<Player *> &p)
  : turn(0), locations(e), actors(c), players(p), items(i) {}


bool Game::checkAction(Action* act) {
  switch(act->kind_of_action) {
  case CHARACTER:
    {
      CharacterAction* cact = (CharacterAction*)act;
      assert(cact->get_actor()!=NULL);
      return checkCharacterAction(cact);
    }
  case ENVIRONMENT:
    return true; // allt ok i nuläget
  case ITEM:
    return false; // inte implementerad i nuläget
  default:
    return false;
  }
}

bool Game::checkCharacterAction(CharacterAction*  cact) {

  CharacterAction & c_act = *cact;  
  Character* chrptr = cact->get_actor();
  Character & chr = *chrptr;

  // http://en.wiktionary.org/wiki/attackee
  Character* attackee = c_act.chr;

  switch (c_act.act_type) {
  case GO:
    assert(c_act.env != NULL);
    return chr.c_current_location->can_enter(chr, c_act.env);

  case TAKE:
    return chr.c_current_location->items.find(c_act.it) != 
      chr.c_current_location->items.end();
        
  case ATTACK:
    // check enemy != attacker and enemy is in environment 

    assert(attackee != NULL);
    if (attackee == &chr) return false; // Får inte attackera en själv

    // Den attackerade finns i rummet.
    return chr.c_current_location->in_current_location.find(attackee)!= chr.c_current_location->in_current_location.end();
  case DROP:
    
    return chr.c_items.find(c_act.it) != 
      chr.c_items.end();
    
  case USE:
    return chr.c_items.find(c_act.it) != 
      chr.c_items.end();
    
  case SAY:
    return true;
  case PASS:
    return true;
  }
  return false;
}

void Game::performAction(Action* act) {
  switch(act->kind_of_action){
  case CHARACTER:
    {
      CharacterAction* cact = (CharacterAction*)act;    
      Character* actorptr = cact->get_actor();
      Character & actor = *actorptr;
      performCharacterAction(actor, *cact);
      break;
    }
  case ENVIRONMENT:
    {
      EnvironmentAction* eact = (EnvironmentAction*)act;
      eact->get_actor()->act();
      std::cout << "Performing " << eact->act << " of environment " << eact->get_actor()
                << std::endl;
      break;
    }
  case ITEM:
  default:
    break;
  }
  delete act;
}

void Game::performCharacterAction(Character & chr, CharacterAction c_act) {
  switch (c_act.act_type) {
  case GO:
    std::cout << chr.to_string() << " entered " << c_act.env << std::endl;
    c_act.env->in_current_location.insert(&chr);
    chr.c_current_location->in_current_location.erase(&chr);
    chr.c_current_location=c_act.env;
    break;
    
  case TAKE:
    std::cout << chr.to_string() << " took " << c_act.it->description()
              << " from " << chr.c_current_location << std::endl;
    chr.c_current_location->items.erase(c_act.it);
    chr.c_items.insert(c_act.it);
    break;
    
  case ATTACK:
    std::cout << chr.to_string() << " attacked " << c_act.chr->to_string()
              << "!" << std::endl;
    break;
    
  case DROP:
    std::cout << chr.to_string() << " dropped " << c_act.it->description()
              << " to " << chr.c_current_location << std::endl;
    chr.c_current_location->items.insert(c_act.it);
    chr.c_items.erase(c_act.it);
    break;
    
  case USE:
    std::cout << chr.to_string() << " used " << c_act.it->description()
              << std::endl;
    std::cout << c_act.it->use_by(chr) << std::endl;
    chr.c_items.erase(c_act.it);
    items.erase(c_act.it);
    c_act.it->~Item();
    break;
    
  case SAY:
    std::cout << chr.to_string() << " says " << 
      c_act.message << " to " << c_act.chr->to_string() << 
      std::endl;
    break;
  case PASS:
    std::cout << chr.to_string() << " did nothing! " << std::endl;
    break;
  }
}


void Game::play_room(Environment* room) {
  const std::set<Character*> chars_at_turn_start = room->in_current_location;
  for (Character * chr: chars_at_turn_start) {
    if (actors.find(chr) == actors.end()) continue; // not dead-check
      for (int attempt=0; attempt<3; attempt++) {
        CharacterAction* ca = chr->action();
	ca->add_actor(chr);
        if (checkAction(ca)) {
          performAction(ca);
          goto end;
        }
        std::cout << "Action of " << chr->to_string()
                  << " failed, attempt=" << attempt 
                  << std::endl;
      }
      std::cout << "Action of " << chr->to_string()
                << " failed, 3 times, action is PASS"
                << std::endl;
      CharacterAction* act = CharacterAction::pass();
      act->add_actor(chr);
      performAction(act);
    }
  end: ;
  turn++;
}

void Game::play() {
  const std::set<Environment*> envs_at_turn_start = locations;
  for (Environment * env : envs_at_turn_start){
    if (locations.find(env) == locations.end()) continue; // rummet har försvunnit
    EnvironmentAction* ea = env->action();
    // enviroment lägger själva till sig som aktörer
    assert(ea->get_actor()!=NULL);
    if (checkAction(ea)) performAction(ea);
    else delete ea;
    play_room(env);
  }
}

void Game::play_threaded() {
  // initialiseras och körs, oberoende av varandra
  std::thread main_thread(&Game::main_run, this); 
  std::thread automatic_thread(&Game::automatic_run, this); 
  std::set<std::thread*>player_threads;
  for (Player * p: players)  {
    std::thread * t = new std::thread(&Player::run, p);
    player_threads.insert(t);
  }
  main_thread.join();
  automatic_thread.join();
  for (auto t : player_threads)
    t->join();
}

void Game::main_run() {
  while(true){ // TODO: byt while(true) till while boolean
    std::this_thread::sleep_for(std::chrono::seconds(1)); 
    if (!q.empty()){
      Action* action = q.front();
      q.pop();
      // kolla så att man inte har dött
      if (action->actor_is_dead()) { delete action; continue; } 
      if (checkAction(action)) {
	// perform_action kör delete
	performAction(action); 
      } else {
	// TODO: meddela att det inte gick att utföra
	delete action; 
      }
    }
  }
}

void Game::automatic_run_room(Environment* room){
  const std::set<Character*> chars_at_turn_start = room->in_current_location;
  for (Character* chr : chars_at_turn_start) {
    if (actors.find(chr) == actors.end()) continue; // hunnit dö
    if (chr->is_player) continue; // ignorera spelare
    CharacterAction * cact = chr->action();
    cact->add_actor(chr);
    enqueue(cact);
  }
}

/* går igenom alla och köar deras action */
void Game::automatic_run() {
  while (true) {
    const std::set<Environment*> envs_at_turn_start = locations;    
    for (Environment * env : envs_at_turn_start) {
      if (locations.find(env) == locations.end()) continue; // rummet har försvunnit
      EnvironmentAction* ea = env->action();
      // enviroment lägger själva till sig som aktörer
      assert(ea->get_actor()!=NULL);
      enqueue(ea);
      automatic_run_room(env);
    }
    std::this_thread::sleep_for(std::chrono::seconds(3)); 
  }
}

/* kan anropas av flera samtidigt, publik  */
void Game::request_action(CharacterAction* action, Character * who) { // action på heap
  action->add_actor(who);
  enqueue(action);
}

// här finns trådskydd
void Game::enqueue(Action* action) {
  mtx.lock();
  q.push(action);
  mtx.unlock();
}
