#include "Environment.hpp"

const std::set<Character *> Environment::get_visible_chars(Character*) const {
  return in_current_location;
}

const std::set<Environment*> Environment::get_visible_adjecent(Character*) const {
  return adjecent;
}



/* kompileringsfel
bool Environment::is_present_character(const Character& who) const{
  return in_current_location.find(&who)!=in_current_location.end();
}
*/


/* 
   Meningen är att klasser som ärver eventuellt skriver 
   över can_enter till t.ex.

   if (e == blue_door) {
     const std::set<Item*> c_items & = c->get_items();
     if (c_items.find(Blue_Key) == c_items.end())
       return false; // Not there!
     else 
       return true; // Hurray!
   }
   else { ...}

   Den enda som default-implementationen gör är att 
   titta efter om 'e' finns i adjecent

 */
bool Environment::can_enter(Character &c, Environment * e) {
  return adjecent.find(e) != adjecent.end();
}

const std::set<Item*> Environment::get_items() const {
  return items;
}



// Default is do nothing
void Environment::act(void) {}

EnvironmentAction* Environment::action() {
  fpointer my_f_pointer = 
    [] (Environment * env) {env->act(); };
  return new EnvironmentAction(my_f_pointer, this);
}
